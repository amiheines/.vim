set nocompatible        " must be the first line
"filetype on
"filetype indent on
filetype plugin on
set laststatus=2
set statusline=%<%f\%h%m%r%=%-20.(line=%l\ \ col=%c%V\ \ totlin=%L%)\ \ \%h%m%r%=%-40(bytval=0x%B,%n%Y%)\%P
set nu
colors transparent
autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p
set mouse=a " use mouse everywhere
let g:ConqueTerm_SendFileKey = '<F5>'
let g:ConqueTerm_SendVisKey = '<F9>'
set hlsearch
set tabstop=2 shiftwidth=2 expandtab
set directory=/tmp
au BufNewFile,BufRead *.ejs set filetype=html
:set autoindent
:set autowrite
command ZF %g/public function/normal! f{zf%
set fileformats=unix,mac,dos

set foldmethod=syntax
set foldlevelstart=99
let javaScript_fold=1         " JavaScript
let php_folding=1             " PHP
let sh_fold_enabled=1         " sh
let vimsyn_folding='af'       " Vim script
nnoremap <Space> za
set pastetoggle=<F10>         " switch to insert mode and press F10 then ctrl+shift+v
